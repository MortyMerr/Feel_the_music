#include "channel.hpp"
#include <QtTest/QTest>
#include <iterator>
#include "defs.hpp"

Channel::Channel(PatternsContainer* patterns) :
  bps_(0)
{
  patterns_ = patterns,
      packed_ = new QGroupBox;
  QHBoxLayout* layout = new QHBoxLayout;
  for(size_t i = 0; i < PATTERNS_IN_CHANNEL; i++){
    QSpinBox* tmp = new QSpinBox;
    tmp->setRange(-1, PATTERNS_N);
    tmp->setValue(-1);
    patternsSelected_.push_back(tmp);
    layout->addWidget(tmp);
  }
  packed_->setLayout(layout);
}

QGroupBox* Channel::getPacked() const
{
  return packed_;
}

void Channel::setBps(unsigned bps)
{
  bps_ = bps;
}

void Channel::play() const
{
  //qDebug() << "Playing channel";
  QVector<int> patternsQueue;
  for(const QSpinBox* p : patternsSelected_){
    patternsQueue.push_back(p->value());
  }

  for(int i = 0; i < patternsQueue.size(); i++){
    if(patternsQueue[i] == -1){
      int j = i;
      while((j < patternsQueue.size())
            && (patternsQueue[j] == -1)){
        j++;
      }
      if(j == patternsQueue.size()){
        return;
      } else {
        if(bps_ > 0){
          QTest::qSleep(bps_ * SAMPLES_RADIO_BUTTON_N);
        }
      }
    } else {
      const Pattern* currentPattern = patterns_->get(patternsQueue[i]);
      if(currentPattern == nullptr){
        qDebug() << "Pattern not found " << patternsQueue[i];
        return;
      }
      currentPattern->play(bps_);
    }
  }
}

void Channel::serialise(QTextStream& fout) const
{
  int i = 0;
  while((i < patternsSelected_.size())
        && (patternsSelected_[i]->value() != - 1)){
    fout << patternsSelected_[i]->value() << ' ';
    i++;
  }
  fout << endl;
}

void Channel::setPatterns(const QString& patterns)
{
  const QStringList splitted = patterns.trimmed().split(" ");
  for(int i = 0; i < splitted.size(); i++){
    patternsSelected_[i]->setValue(splitted[i].toInt());
  }
}
