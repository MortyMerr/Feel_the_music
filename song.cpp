#include "song.hpp"
#include "defs.hpp"
#include <QThread>

Song::Song(PatternsContainer* patterns) :
  patterns_(patterns)
{
}

bool Song::addChannel()
{
  if(channels_.size() < CHANNELS_MAX){
    channels_.push_back(std::shared_ptr<Channel>(new Channel(patterns_)));
    return true;
  }
  return false;
}

size_t Song::size() const
{
  return channels_.size();
}

Channel* Song::get(size_t i) const
{
  if(i < (size_t)channels_.size()){
    return channels_[i].get();
  }
  return nullptr;
}

void Song::play(unsigned bps) const
{
  for(const auto selectedChannel : channels_){
    selectedChannel->setBps(bps);
    if(selectedChannel != nullptr){
      QThread* thread = new QThread;
      connect(thread, SIGNAL(started()), selectedChannel.get(), SLOT(play()));
      selectedChannel->moveToThread(thread);
      thread->start();
    }
  }
}

void Song::serialise(QTextStream& fout) const
{
  fout << "Song" << endl;
  for(size_t i = 0; i < (size_t)channels_.size(); i++){
    fout << "Channel " << i << endl;
    channels_[i]->serialise(fout);
  }
  fout << endl;
}
