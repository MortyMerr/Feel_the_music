#-------------------------------------------------
#
# Project created by QtCreator 2017-06-04T17:55:35
#
#-------------------------------------------------

QT       += core gui testlib
CONFIG   += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = bass_dll
TEMPLATE = app

LIBS += -L"C:\Users\adm\Documents\qt\build-bass_dll-Desktop_Qt_5_2_1_MinGW_32bit-Debug\debug" -lbass

SOURCES += main.cpp\
        mainwindow.cpp \
    sample.cpp \
    channel.cpp \
    pattern.cpp \
    patternscontainer.cpp \
    song.cpp

HEADERS  += mainwindow.h \
    bass.h \
    sample.hpp \
    defs.hpp \
    channel.hpp \
    pattern.hpp \
    patternscontainer.hpp \
    song.hpp

FORMS    += mainwindow.ui

RESOURCES += \
    res.qrc
