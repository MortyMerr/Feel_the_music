#include "pattern.hpp"
#include <iterator>
#include <QDebug>
#include <QTime>
#include <QtTest/QTest>
#include "defs.hpp"

Pattern::Pattern() :
  loopMode_(LOOP_MOD_MAX)
{
}

void Pattern::addSample(const QString& filename)
{
  samples_.push_back(std::shared_ptr<Sample>(new Sample(filename)));
}

size_t Pattern::size() const
{
  return samples_.size();
}

void Pattern::play(unsigned bps) const
{
  QVector<Sample*> sorted;
  for(auto sample : samples_){
    sorted.push_back(sample.get());
  }

  QVector <QVector <Sample*> > steps(loopMode_);
  for(Sample* sample : sorted){
    for(int i = 0; i < steps.size(); i++){
      if(sample->isPosSelected(i)){
        steps[i].push_back(sample);
      }
    }
  }

  for(size_t i = 0; i < LOOP_MOD_MAX / loopMode_; i++){
    for(QVector<Sample*>& step : steps){
      const QTime startStep = QTime::currentTime();
      for(Sample* sample : step){
        sample->play();
      }
      const unsigned pause = bps - startStep.elapsed();
      if(pause > 0){//if played faster then bps
        QTest::qSleep(pause);
      }
    }
  }
}

Sample* Pattern::get(size_t sampleNum) const
{
  return samples_[sampleNum].get();
}

void Pattern::serialise(QTextStream& fout) const
{
  for(size_t i = 0; i < samples_.size(); i++){
    fout << "Sample " << i << endl;
    samples_[i]->serialise(fout);
  }
}

Pattern::~Pattern()
{
  qDebug() << "Pattern destroyed";
}

void Pattern::setLoopMode(unsigned loopMode)
{
  loopMode_ = loopMode;
  for(auto sample : samples_){
    sample->setLoopMode(loopMode_);
  }
}
