#ifndef TABLESCONTAINER_HPP
#define TABLESCONTAINER_HPP

#include "pattern.hpp"

class PatternsContainer
{
public:
  PatternsContainer();
  ~PatternsContainer();
  void addSample(const QString& filename, size_t tableNum);
  size_t size() const;
  Pattern* get(size_t i) const;
  Sample* getLastAddedSample() const;
  void serialise(QTextStream& fout) const;
public:
  std::vector <std::shared_ptr<Pattern>> patterns_;
  Sample* lastAddedSample_;
};

#endif // TABLESCONTAINER_HPP
