#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMainWindow>
#include <QCloseEvent>
#include <QFileDialog>
#include <QMessageBox>
#include <QString>
#include <QLabel>
#include <QWidget>
#include <QDebug>
#include <QThread>
#include <string>
#include "bass.h"
#include "defs.hpp"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  selectedPattern_(0),
  patternsContainer_(std::shared_ptr<PatternsContainer>(new PatternsContainer())),
  song_(std::unique_ptr<Song>(new Song(patternsContainer_.get())))
{
  ui->setupUi(this);
  QPixmap pixmap(":/images/plus.png");
  pixmap = pixmap.scaledToWidth(ADD_BUTTON_WIDTH);
  ui->addSample->setIcon(pixmap);
  ui->addSample->setIconSize(pixmap.rect().size());

  ui->addChannel->setIcon(pixmap);
  ui->addChannel->setIconSize(pixmap.rect().size());

  ui->patternSelector->setRange(0, 9);

  ui->volumeSlider->setValue(100);
  ui->bpmSlider->setRange(50, 200);
  ui->bpmSlider->setValue(180);

  ui->volumeDisplay->display(ui->volumeSlider->value());
  ui->bpmDisplay->display(ui->bpmSlider->value());

  for(size_t i = 1; i <= PATTERNS_IN_CHANNEL; i++){
    QLabel* tmp = new QLabel(QString::number(i));
    ui->nums->addWidget(tmp);
  }

  for(size_t i = LOOP_MOD_MAX; i > 1; i /= 2){
    ui->loopMode->addItem(QString::number(i));
  }
  //test
  //test();
}

void MainWindow::test()
{
  QVector<QString> testSamples;
  testSamples.push_back("C:/Users/adm/Documents/qt/build-bass_dll-Desktop_Qt_5_2_1_MinGW_32bit-Debug/DHN2_Hat_08_FRK.wav");
  testSamples.push_back("C:/Users/adm/Documents/qt/build-bass_dll-Desktop_Qt_5_2_1_MinGW_32bit-Debug/DHN2_Kick_01_FRK.wav");
  testSamples.push_back("C:/Users/adm/Documents/qt/build-bass_dll-Desktop_Qt_5_2_1_MinGW_32bit-Debug/drum1.wav");
  if(patternsContainer_->patterns_[0] == nullptr){
    patternsContainer_->patterns_[0] = std::shared_ptr<Pattern>(new Pattern());
  }
  for(const QString& string : testSamples){
    patternsContainer_->patterns_[0]->addSample(string);
    Sample* lastAdded  = patternsContainer_->patterns_[0]->
                         get(patternsContainer_->patterns_[0]->size() - 1);
    ui->samplesTable->addWidget(lastAdded->getPacked());
  }
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::on_addSample_clicked()
{
  const QString filename = QFileDialog::getOpenFileName(nullptr, QObject::tr("Select sample"),
                                                        QDir::currentPath(),
                                                        QObject::tr("wav Files (*.wav)"));
  if(filename == ""){
    qDebug() << "No file selected";
    return;
  }
  patternsContainer_->addSample(filename, selectedPattern_);
  Sample* lastAdded  = patternsContainer_->getLastAddedSample();
  ui->samplesTable->addWidget(lastAdded->getPacked());
}

void MainWindow::clearWidgetsFromLayout()
{
  const Pattern* selectedTable = patternsContainer_->get(selectedPattern_);
  if(selectedTable != nullptr){
    for(size_t i = 0; i < selectedTable->size(); i++){
      auto tmp = ui->samplesTable->takeAt(0);
      if(tmp == 0){
        return;
      }
      tmp->widget()->setParent(0);
    }
  }
}

void MainWindow::clearChannels()
{
  for(size_t i = 0; i < song_->size(); i++){
    auto tmp = ui->channels->takeAt(0);
    if(tmp == 0){
      return;
    }
    tmp->widget()->setParent(0);
  }
}

void MainWindow::reloadSamplesTable()
{
  const Pattern* selectedTable = patternsContainer_->get(selectedPattern_);
  if(selectedTable != nullptr){
    for(size_t i = 0; i < selectedTable->size(); i++){
      ui->samplesTable->addWidget(selectedTable->get(i)->getPacked());
    }
  }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
  BASS_Free();
  qDebug() << "Bass free";
  event->accept();
}

void MainWindow::on_patternSelector_valueChanged(int arg1)
{
  clearWidgetsFromLayout();
  selectedPattern_ = arg1;
  reloadSamplesTable();
}

void MainWindow::on_addChannel_clicked()
{
  if(song_->addChannel()){
    ui->channels->addWidget(song_->get(song_->size() - 1)->getPacked());
  }
}

void MainWindow::on_volumeSlider_sliderMoved(int position)
{
  ui->volumeDisplay->display(QString::number(position));
  BASS_SetConfig(BASS_CONFIG_GVOL_SAMPLE, position * 100);
}

void MainWindow::on_bpmSlider_sliderMoved(int position)
{
  ui->bpmDisplay->display(QString::number(position));
}

void MainWindow::on_playPattern_clicked()
{
  const unsigned bps = 60 / (double)(ui->bpmSlider->sliderPosition()) * 1000;
  const auto patternToPlay = patternsContainer_->get(selectedPattern_);
  if (patternToPlay != nullptr){
    patternToPlay->play(bps);
  }
}

void MainWindow::on_playSong_clicked()
{
  const unsigned bps = 30 / (double)(ui->bpmSlider->sliderPosition()) * 1000;
  song_->play(bps);
}

void MainWindow::on_playChannel_clicked()
{
  Channel* selectedChannel = song_->get(ui->selectChannel->value());
  const unsigned bps = 60 / (double)(ui->bpmSlider->sliderPosition()) * 1000;
  selectedChannel->setBps(bps);
  if(selectedChannel != nullptr){
    QThread* thread = new QThread;
    connect(thread, SIGNAL(started()), selectedChannel, SLOT(play()));
    thread->start();
  }
}

void MainWindow::on_loopMode_currentTextChanged(const QString &arg1)
{
  const auto currentPattern = patternsContainer_->get(selectedPattern_);
  if(currentPattern != nullptr){
    currentPattern->setLoopMode(arg1.toInt());
  }
}

void MainWindow::on_loadFromFile_clicked()
{
  clearAll();
  patternsContainer_.reset(new PatternsContainer());
  song_.reset(new Song(patternsContainer_.get()));

  const QString filename = QFileDialog::getOpenFileName(nullptr, QObject::tr("Open"),
                                                        QDir::currentPath(),
                                                        QObject::tr("txt Files (*.txt)"));
  const QDir dir = QFileInfo(filename).absoluteDir();
  const QString pathToSample = dir.absolutePath();
  if(filename == ""){
    qDebug() << "No file selected";
    return;
  }

  QFile fileIn(filename);
  if(fileIn.open(QIODevice::ReadOnly | QIODevice::Text)){
    QTextStream fin(&fileIn);
    QString tmp = "";
    fin.readLine();
    for(size_t i = 0; i < PATTERNS_N; i++){
      tmp = fin.readLine();
      if(tmp.left(7) == "Pattern"){
        continue;
      }
      do{
        tmp = fin.readLine();
        try{
        patternsContainer_->addSample(pathToSample + '/' + tmp, i);
        } catch (const std::invalid_argument& e)
        {
          QMessageBox::critical(NULL, QObject::tr("Ошибка"), e.what());
          return;
        }

        Sample* lastAdded = patternsContainer_->getLastAddedSample();
        tmp = fin.readLine();
        lastAdded->setVolume(tmp.toInt());
        tmp = fin.readLine();
        lastAdded->setLoopMode(tmp.toInt());
        tmp = fin.readLine();
        lastAdded->setPos(tmp);
        tmp = fin.readLine();
        if(i == 0){
          ui->samplesTable->addWidget(lastAdded->getPacked());
        }
      }
      while (tmp.left(7) != "Pattern");
    }

    tmp = fin.readLine();
    tmp = fin.readLine();
    tmp = fin.readLine();
    while(!fin.atEnd()){
      song_->addChannel();
      tmp = fin.readLine();
      song_->get(song_->size() - 1)->setPatterns(tmp);
      ui->channels->addWidget(song_->get(song_->size() - 1)->getPacked());
      tmp = fin.readLine();
    }
    fileIn.close();
  }
}

void MainWindow::on_saveToFile_clicked()
{
  const QString filename = QFileDialog::getSaveFileName(nullptr, QObject::tr("Save to"),
                                                        QDir::currentPath(),
                                                        QObject::tr("txt Files (*.txt)"));
  if(filename == ""){
    qDebug() << "No file selected";
    return;
  }
  QFile file(filename);
  if(file.open(QIODevice::WriteOnly | QIODevice::Text)){
    QTextStream fout(&file);
    patternsContainer_->serialise(fout);
    fout << endl;
    song_->serialise(fout);
    file.close();
  }
}

void MainWindow::clearAll()
{
  clearWidgetsFromLayout();
  clearChannels();
}

void MainWindow::on_clearAll_clicked()
{
  clearAll();
}
