#include "mainwindow.h"
#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  if (!BASS_Init(-1, 44100, 0, 0, NULL)){
    qDebug() << "Can't initialize device";
  }
  MainWindow w;
  w.show();

  return a.exec();
}
