#ifndef DEFS_H
#define DEFS_H

#define PATTERNS_N 10
#define SAMPLES_N 10
#define SAMPLES_RADIO_BUTTON_N 16
#define ADD_BUTTON_WIDTH 30
#define PATTERNS_IN_CHANNEL 16
#define CHANNELS_MAX 6
#define LOOP_MOD_MAX 16


#endif // DEFS_H
