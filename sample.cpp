#include "sample.hpp"
#include <stdexcept>
#include <QDebug>
#include <QHBoxLayout>
#include "defs.hpp"

Sample::Sample(const QString& filename) :
  path_(filename),
  loopMode_(LOOP_MOD_MAX)
{
  const std::string tmp = filename.toStdString();//FIXME without this - it crashes.
  const char* c_string = tmp.c_str();
  const bool loadResult =
      (sample_ = BASS_SampleLoad(FALSE, c_string, 0, 0, 3, BASS_SAMPLE_OVER_POS));

  if (!loadResult){
    throw std::invalid_argument("Error while loading sample: \n"
                                + path_.toStdString()
                                + "\nFile wasn't load. Please resolve conflicts and reload file.");
  }
  const QStringList splitRes = filename.split("/");
  name_ = *(splitRes.end() - 1);
  qDebug() << "Sample " << name_ << "load successful";
  createWidgetElements();
}

void Sample::createWidgetElements()
{
  packed_ = new QGroupBox(name_);
  volume_.reset(new QSlider(Qt::Orientation::Horizontal));
  connect(volume_.get(), SIGNAL(valueChanged(int)), this, SLOT(volumeChanged(int)));
  volume_->setValue(100);

  QHBoxLayout* tmpLayout = new QHBoxLayout();
  tmpLayout->setSizeConstraint(QLayout::SetNoConstraint);
  QPushButton* playButton = new QPushButton("Play");
  connect(playButton, SIGNAL(clicked()), this, SLOT(play()));

  tmpLayout->addWidget(playButton);
  tmpLayout->addWidget(volume_.get());
  for(size_t i = 0; i < SAMPLES_RADIO_BUTTON_N; i++){
    QCheckBox* tmp = new QCheckBox();
    tmpLayout->addWidget(tmp);
    pos_.push_back(std::unique_ptr<QCheckBox>(tmp));
  }
  packed_->setLayout(tmpLayout);
}

QGroupBox* Sample::getPacked() const
{
  return packed_;
}

Sample::~Sample()
{
  qDebug() << "Sample destroyed " << path_;
  BASS_SampleFree(sample_);
}

bool Sample::isPosSelected(size_t i) const
{
  return pos_[i]->isChecked();
}

HSAMPLE Sample::getSampleHandle() const
{
  return sample_;
}

unsigned Sample::getVolume() const
{
  return volume_->sliderPosition();
}

void Sample::volumeChanged(int value)
{
  const float currentVolume = (float)(value) / 100;
  qDebug() << "Current volume set on"
           << QString::number(currentVolume);
  BASS_ChannelSetAttribute(ch_, BASS_ATTRIB_VOL, currentVolume);
}

void Sample::play()
{
  ch_ =BASS_SampleGetChannel(sample_,FALSE);
  //TODO change to real change
  BASS_ChannelSetAttribute(ch_, BASS_ATTRIB_VOL,
                           ((float)(volume_->sliderPosition()) / 100));
  BASS_ChannelSetAttribute(ch_, BASS_ATTRIB_PAN, ((rand() % 201) - 100) / 100.f);
  if (!BASS_ChannelPlay(ch_, FALSE)){
    qDebug() << "Can't play sample";
  }
}

void Sample::serialise(QTextStream& fout) const
{
  fout << name_ << endl;
  fout << volume_->value() << endl;
  fout << loopMode_ << endl;
  for(size_t i = 0; i < pos_.size(); i++){
    fout << pos_[i]->isChecked() << ' ';
  }
  fout << endl;
}

void Sample::setLoopMode(unsigned loopMode)
{
  loopMode_ = loopMode;
  for(size_t i = 0; i < loopMode_; i++){
    pos_[i]->setVisible(true);
  }
  for(size_t i = loopMode_;i < pos_.size(); i++){
    pos_[i]->setVisible(false);
  }
}

void Sample::setVolume(unsigned volume)
{
  volume_->setValue(volume);
}

void Sample::setPos(const QString& pos)
{
  const QStringList splitted = pos.split(" ");
  for(size_t i = 0; i < pos_.size(); i++){
    pos_[i]->setChecked(splitted[i].toInt());
  }
}
