#ifndef PATTERN_HPP
#define PATTERN_HPP

#include <vector>
#include "sample.hpp"

class Pattern
{
public:
  Pattern();
  ~Pattern();
  void addSample(const QString& filename);
  Sample* get(size_t sampleNum) const;
  size_t size() const;
  void play(unsigned bps) const;
  void serialise(QTextStream& fout) const;
  void setLoopMode(unsigned loopMode);
private:
  std::vector <std::shared_ptr<Sample>> samples_;
  unsigned loopMode_;
};

#endif // PATTERN_HPP
