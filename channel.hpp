#ifndef CHANNEL_HPP
#define CHANNEL_HPP
//TOUSER don't know how should i call one string from down table, so it will be "channel"

#include <QSpinBox>
#include <QHBoxLayout>
#include <QObject>
#include "pattern.hpp"
#include "patternscontainer.hpp"

class Channel : public QObject
{
  Q_OBJECT
public:
  Channel(PatternsContainer* patterns);
  QGroupBox* getPacked() const;
  void setBps(unsigned bps);
  void serialise(QTextStream& fout) const;
  void setPatterns(const QString& patterns);
public slots:
  void play() const;
private:
  PatternsContainer* patterns_;
  QVector <QSpinBox*> patternsSelected_;
  QGroupBox* packed_;
  unsigned bps_;
};

#endif // CHANNEL_HPP
