#include "patternscontainer.hpp"
#include <QFileDialog>
#include <QString>
#include <QDebug>
#include <QString>
#include <QLabel>
#include <QWidget>
#include <QDebug>
#include <string>
#include <QObject>
#include "defs.hpp"

PatternsContainer::PatternsContainer() :
  patterns_(std::vector <std::shared_ptr<Pattern>>(PATTERNS_N))
{
}

void PatternsContainer::addSample(const QString& filename, size_t tableNum)
{
  if((patterns_[tableNum] == nullptr)
     || (patterns_[tableNum]->size() < SAMPLES_N)){
    if(patterns_[tableNum] == nullptr){
      patterns_[tableNum] =
          std::shared_ptr<Pattern>(new Pattern());
    }
    patterns_[tableNum]->addSample(filename);
    lastAddedSample_ = patterns_[tableNum]->get(patterns_[tableNum]->size() - 1);
  }
}


Sample* PatternsContainer::getLastAddedSample() const
{
  return lastAddedSample_;
}

size_t PatternsContainer::size() const
{
  return patterns_.size();
}

Pattern* PatternsContainer::get(size_t i) const
{
  return patterns_[i].get();
}

void PatternsContainer::serialise(QTextStream& fout) const
{
  for(size_t i = 0; i < patterns_.size(); i++){
    fout << "Pattern " << i << endl;
    if(patterns_[i] != nullptr){
      patterns_[i]->serialise(fout);
    }
  }
  fout << "Pattern end" << endl;
}

PatternsContainer::~PatternsContainer()
{
  lastAddedSample_ = nullptr;
  qDebug() << "Pattertns container destroyed";
}
