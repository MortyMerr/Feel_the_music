#ifndef SONG_HPP
#define SONG_HPP

#include "channel.hpp"
#include "patternscontainer.hpp"
class Song : public QObject
{
  Q_OBJECT
public:
  Song(PatternsContainer* patterns);
  bool addChannel();
  Channel* get(size_t i) const;
  size_t size() const;
  void play(unsigned bps) const;
  void serialise(QTextStream& fout) const;
private:
  PatternsContainer* patterns_;
  QVector <std::shared_ptr<Channel>> channels_;
};

#endif // SONG_HPP
