#ifndef SAMPLE_H
#define SAMPLE_H

#include <string>
#include <QButtonGroup>
#include <QSlider>
#include <QGroupBox>
#include <QPushButton>
#include <QCheckBox>
#include <QTextStream>
#include <memory>
#include "bass.h"

class Sample : public QObject
{
  Q_OBJECT
public:
  Sample(const QString& filename);
  QGroupBox* getPacked() const;
  bool isPosSelected(size_t i) const;
  QPushButton* getPlayButton() const;
  HSAMPLE getSampleHandle() const;
  unsigned getVolume() const;
  void serialise(QTextStream& fout) const;
  void setLoopMode(unsigned loopMode);
  void setVolume(unsigned volume);
  void setPos(const QString& pos);
  ~Sample();
public slots:
  void volumeChanged(int value);
  void play();
private:
  void createWidgetElements();
  QString path_, name_;
  std::unique_ptr<QSlider> volume_;
  HSAMPLE sample_;
  QGroupBox* packed_;
  std::vector <std::unique_ptr<QCheckBox>> pos_;
  unsigned loopMode_;
  mutable HCHANNEL ch_;
};

#endif // SAMPLE_H
