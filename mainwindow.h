#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include "patternscontainer.hpp"
#include "song.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
  void playSound();
private slots:

  void on_addSample_clicked();
  void on_patternSelector_valueChanged(int arg1);

  void on_addChannel_clicked();

  void on_volumeSlider_sliderMoved(int position);

  void on_bpmSlider_sliderMoved(int position);

  void on_playPattern_clicked	();

  void on_playSong_clicked();

  void on_playChannel_clicked();

  void on_loopMode_currentTextChanged(const QString &arg1);

  void on_loadFromFile_clicked();

  void on_saveToFile_clicked();

  void on_clearAll_clicked();

private:
  Ui::MainWindow *ui;
  size_t selectedPattern_;
  std::shared_ptr<PatternsContainer> patternsContainer_;
  std::unique_ptr<Song> song_;

  void clearAll();
  void test();
  void reloadSamplesTable();
  void clearWidgetsFromLayout();
  void clearChannels();

  void closeEvent(QCloseEvent* event) override;
};

#endif // MAINWINDOW_H
